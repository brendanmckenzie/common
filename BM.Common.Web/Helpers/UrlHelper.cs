﻿using System;
using System.Web.Mvc;

namespace BM.Common.Web.Helpers
{
    /// <summary>
    /// Helper class for Url based functions.
    /// </summary>
    public static class Url
    {
        /// <summary>
        /// Gets the base Url given the <see cref="UrlHelper" />.
        /// </summary>
        /// <param name="url">The <see cref="UrlHelper" /> object.</param>
        /// <returns>
        /// The base Url given for the given <see cref="UrlHelper" />.
        /// </returns>
        public static Uri GetBaseUrl(this UrlHelper url)
        {
            var uri = new Uri(
                url.RequestContext.HttpContext.Request.Url,
                url.RequestContext.HttpContext.Request.RawUrl
            );
            var builder = new UriBuilder(uri)
            {
                Path = url.RequestContext.HttpContext.Request.ApplicationPath,
                Query = null,
                Fragment = null
            };
            return builder.Uri;
        }

        /// <summary>
        /// Gets the absolute Url of content.
        /// </summary>
        /// <param name="url">The <see cref="UrlHelper" /> object.</param>
        /// <param name="contentPath">The content path.</param>
        /// <returns></returns>
        public static string ContentAbsolute(this UrlHelper url, string contentPath)
        {
            return new Uri(GetBaseUrl(url), url.Content(contentPath)).AbsoluteUri;
        }
    }
}
