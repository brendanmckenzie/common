﻿using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace BM.Common.Web.Filters
{
    /// <summary>
    /// Extends the functionality of <see cref="AuthorizeAttribute" /> by adding checking for roles and responding with 403 instead of 401 to avoid possible redirect loops.
    /// </summary>
    public class AuthoriseAttribute : FilterAttribute, IAuthorizationFilter
    {
        #region Enums

        enum AuthorisationResult
        {
            NotAuthenticated,
            NotAuthorised,
            Authorised
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the roles a user must have to satisfy authorisation.
        /// </summary>
        /// <value>
        /// The roles a user must have to satisfy authorisation.
        /// </value>
        public string Roles
        {
            get { return _roles ?? String.Empty; }
            set
            {
                _roles = value;
                _rolesSplit = SplitString(value);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Called when authorization is required.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        /// <exception cref="System.ArgumentNullException">filterContext</exception>
        /// <exception cref="System.InvalidOperationException"></exception>
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (OutputCacheAttribute.IsChildActionCacheActive(filterContext))
            {
                // If a child action cache block is active, we need to fail immediately, even if authorization
                // would have succeeded. The reason is that there's no way to hook a callback to rerun
                // authorization before the fragment is served from the cache, so we can't guarantee that this
                // filter will be re-run on subsequent requests.
                throw new InvalidOperationException();
            }

            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                                     || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (skipAuthorization)
            {
                return;
            }

            var authResult = PerformAuthorisation(filterContext.HttpContext);

            switch (authResult)
            {
                case AuthorisationResult.Authorised:
                    {
                        HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
                        cachePolicy.SetProxyMaxAge(new TimeSpan(0));
                        cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
                    }
                    break;
                case AuthorisationResult.NotAuthenticated:
                    {
                        filterContext.Result = new HttpUnauthorizedResult();
                    }
                    break;
                case AuthorisationResult.NotAuthorised:
                    filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden);
                    break;
            }
        }

        #endregion

        #region Private Methods

        // This method must be thread-safe since it is called by the thread-safe OnCacheAuthorization() method.
        AuthorisationResult PerformAuthorisation(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            var user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return AuthorisationResult.NotAuthenticated;
            }

            if (_rolesSplit.Length > 0 && !_rolesSplit.Any(user.IsInRole))
            {
                return AuthorisationResult.NotAuthorised;
            }

            return AuthorisationResult.Authorised;
        }

        void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
        {
            validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
        }

        // This method must be thread-safe since it is called by the caching module.
        HttpValidationStatus OnCacheAuthorization(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }

            var authResult = PerformAuthorisation(httpContext);
            return (authResult == AuthorisationResult.Authorised) ? HttpValidationStatus.Valid : HttpValidationStatus.IgnoreThisRequest;
        }

        static string[] SplitString(string original)
        {
            if (String.IsNullOrEmpty(original))
            {
                return new string[0];
            }

            var split = from piece in original.Split(',')
                        let trimmed = piece.Trim()
                        where !String.IsNullOrEmpty(trimmed)
                        select trimmed;
            return split.ToArray();
        }

        #endregion

        #region Private Fields

        private string _roles;
        private string[] _rolesSplit = new string[0];

        #endregion
    }
}