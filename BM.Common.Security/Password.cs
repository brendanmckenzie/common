﻿using CryptSharp.Utility;
using System.Text;
using System.Linq;

namespace BM.Common.Security
{
    /// <summary>
    /// Provides basic secure password encrypting and checking functions.
    /// </summary>
    public static class Password
    {
        /// <summary>
        /// Generates a password given the specified salt and unencrypted password.
        /// </summary>
        /// <param name="salt">The salt.</param>
        /// <param name="password">The password.</param>
        /// <returns>The encrypted copy of the passowrd.</returns>
        public static byte[] Generate(byte[] salt, string password)
        {
            var output = new byte[64];

            SCrypt.ComputeKey(key: Encoding.UTF8.GetBytes(password),
                salt: salt,
                cost: 16,
                blockSize: 8,
                parallel: 1,
                maxThreads: 8,
                output: output);

            return output;
        }
    }
}
