@echo off

SET PACKAGE=BM.Common.Web

echo.
echo *** Removing old packages
del *.nupkg

echo.
echo *** Building package
nuget pack %PACKAGE%.csproj -Prop Configuration=Release

echo.
echo *** Publishing
nuget push *.nupkg -s http://nuget.brendanmckenzie.com/ 12B270DC-803A-4C80-9618-BD73AC1017B2

echo.
echo *** Tidying up
del *.nupkg
echo.
@echo on