﻿using System.Data.Entity.Design.PluralizationServices;
using System.Globalization;

namespace BM.Common.Web.Helpers
{
    /// <summary>
    /// Helper class for pluralising strings.
    /// </summary>
    public static class PluralisationHelper
    {
        /// <summary>
        /// Pluralises the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="count">The count.</param>
        /// <returns></returns>
        public static string Pluralise(this string value, int count)
        {
            if (count == 1)
            {
                return value;
            }

            return PluralizationService
                .CreateService(CultureInfo.CurrentCulture)
                .Pluralize(value);
        }
    }
}
