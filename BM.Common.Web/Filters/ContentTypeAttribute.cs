﻿using System.Web.Mvc;

namespace BM.Common.Web.Filters
{
    /// <summary>
    /// Provides the ability to specify the HTTP Content-Type header of an action.
    /// </summary>
    public class ContentTypeAttribute : ActionFilterAttribute
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentTypeAttribute" /> class.
        /// </summary>
        /// <param name="contentType">Type of the content.</param>
        public ContentTypeAttribute(string contentType)
        {
            _contentType = contentType;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Called by the ASP.NET MVC framework after the action result executes.
        /// </summary>
        /// <param name="filterContext">The filter context.</param>
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.ContentType = _contentType;
        }

        #endregion

        #region Private Fields

        string _contentType { get; set; }

        #endregion
    }
}